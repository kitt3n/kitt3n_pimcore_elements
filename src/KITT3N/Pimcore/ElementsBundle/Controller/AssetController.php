<?php

namespace KITT3N\Pimcore\ElementsBundle\Controller;

use AppBundle\AppBundle;
use Exception;
use phpDocumentor\Reflection\Types\Boolean;
use Pimcore\Config;
use Pimcore\Controller\FrontendController;
use Pimcore\Model\Asset;
use Pimcore\Model\Asset\Listing as Listing;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

class AssetController extends FrontendController
{

    /**
     * @param Request $request
     */
    public function simpleFileBrowserAction(Request $request)
    {
        $this->view->oUser = $this->getUser();

        $this->view->aListOfAssets = [];
        $this->view->sEditmode = "";

        $aMessages  = [];
        $this->view->aMessages = $aMessages;

        $aCrumbs  = [];
        $this->view->aCrumbs = $aCrumbs;

        /*
         * if object dropped to ´Renderlet' is not an asset
         * => return
         */
        if ("asset" !== $request->get("type")) {
            return;
        }

        /*
         * ID of the asset pasted into 'Renderlet'
         */
        $iRenderletAssetId = $request->get("id");

        /*
         * Asset pasted into 'Renderlet'
         */
        $oRenderletAsset = Asset::getById($iRenderletAssetId);
        $oCurrentAsset = $oRenderletAsset;

        /*
         * if object dropped to ´Renderlet' is not an asset or not folder
         * => return
         */
        if ( ! $oRenderletAsset instanceof Asset or "folder" !== $oRenderletAsset->getType()) {
            return;
        }

        /*
         * $aAllParams passes (_GET) parameters in the content element
         */
        $aAllParams = $request->attributes->get('aAllParams');
        if ( ! empty($aAllParams) && array_key_exists("id", $aAllParams)) {

            /*
             * Override current asset if _GET[id] is set
             */
            $oCurrentAsset = Asset::getById(intval($aAllParams["id"]));

            /*
             * if current asset is not an asset or not folder
             * => return
             */
            if ( ! $oCurrentAsset instanceof Asset or "folder" !== $oCurrentAsset->getType()) {
                return;
            }

        }

        if ($oRenderletAsset !== $oCurrentAsset) {

            /*
             * if object dropped to ´Renderlet' does not contain current asset
             * => return
             */
            if ( ! $this->renderletAssetContainsAsset($oRenderletAsset, $oCurrentAsset)) {
                $this->addMessage( $aMessages, "info", "Folder not (longer) available.");
                $this->view->aMessages = $aMessages;
                return;
            }

        }

        /*
         * Get children for current asset
         */
        $sAssetFullPathForEditmode = $oCurrentAsset->getFullPath();
        $this->view->sEditmode = $sAssetFullPathForEditmode;

        /*
         * Get children with all types for current asset
         */
        $aListOfChildren = $this->getChildren($oCurrentAsset, []);
        $this->view->aListOfChildren = $aListOfChildren;

        $aSupportedAssetTypes = [
            'folder',
            'document',
            'image',
            'text'
        ];
        $aListOfAssets = [];
        foreach ($aSupportedAssetTypes as $sType) {
            /* @var Asset $oAsset */
            foreach ($aListOfChildren as $iKey => $oAsset) {
                if ($oAsset->getType() === $sType) {
                    array_push($aListOfAssets, $oAsset);
                    unset($aListOfChildren[$iKey]);
                }
            }
        }
        $this->view->aListOfAssets = $aListOfAssets;

        /*
         * Fill breadcrumb array
         */
        $this->addCrumbsToRootline($aCrumbs, $oRenderletAsset, $oCurrentAsset);
        $this->view->aCrumbs = $aCrumbs;

//        $tag = \Pimcore\Model\Element\Tag::getById(3);
//        $e = \Pimcore\Model\Element\Tag::getElementsForTag($tag,'asset', ['document', 'image','text']);
//        $x=1;
    }

    /**
     * @return array
     */
    protected function getChildren(Asset $oAsset, array $aTypes = [])
    {
        $list = new Asset\Listing();
        switch (true) {
            case ! empty($aTypes):
                $sConditions = ' AND type IN (' . implode(",", str_replace($aTypes, "?", $aTypes)) . ')';
                $aConditions = [$oAsset->getId()];
                foreach ($aTypes as $sType) {
                    array_push($aConditions, $sType);
                }
                $list->setCondition('parentId = ?' . $sConditions, $aConditions);
                break;
            default:
                $list->setCondition('parentId = ?', [$oAsset->getId()]);
        }

        $list->setOrderKey(['type', 'filename']);
        $list->setOrder(['asc','asc']);

        return $list->load();
    }

    protected function addCrumbsToRootline (array &$aCrumbs, Asset $oRenderletAsset, Asset $oAsset) {

        if ($oRenderletAsset === $oAsset) {

            array_unshift(
                $aCrumbs,
                [
                    "label" => $oAsset->getFilename(),
                    "id" => $oAsset->getId(),
                ]
            );
            return;

        }

        $sAssetFullPath = $oAsset->getFullPath();
        $sRenderletFullPath = $oRenderletAsset->getFullPath();
        if (strpos($sAssetFullPath, $sRenderletFullPath) !== false) {

            array_unshift(
                $aCrumbs,
                [
                    "label" => $oAsset->getFilename(),
                    "id" => $oAsset->getId(),
                ]
            );

            $oParentAsset = $oAsset->getParent();
//            if ($oRenderletAsset !== $oParentAsset) {
            $this->addCrumbsToRootline($aCrumbs, $oRenderletAsset, $oParentAsset);

        }
        return;
    }

    protected function addMessage(
        array &$aMessages,
        string $sType,
        string $sMessage
    ) {
        array_push(
            $aMessages,
            [
                "type" => $sType,
                "message" => $sMessage,
            ]
        );
    }

    protected function renderletAssetContainsAsset (Asset $oRenderletAsset, Asset $oAsset) {

        $sAssetFullPath = $oAsset->getFullPath();
        $sRenderletFullPath = $oRenderletAsset->getFullPath();

        if (substr($sAssetFullPath, 0, strlen($sRenderletFullPath)) === $sRenderletFullPath) {
            return true;
        }

        return false;

    }

    /**
     * @param Request $request
     * @Route("/simple-file-viewer/asset/{id}", name="asset_details", requirements={"id"="\d+"})
     */
    public function simpleFileViewerAction(Request $request)
    {
        //echo "Simple file viewer";
        $aTagsForCurrentAsset =  \Pimcore\Model\Element\Tag::getTagsForElement('asset', $request->get("id"));
        $this->view->aTagsForCurrentAsset = $aTagsForCurrentAsset;

        $this->view->oAsset = Asset::getById($request->get('id'));
    }

    /**
     * @Route("/simple-file-downloader/asset/{id}", name="download_an_asset", requirements={"id"="\d+"})
     *
     * @param Request $request
     *
     * @return BinaryFileResponse
     */
    public function simpleFileDownloaderAction(Request $request)
    {
        if ($this->isAllowed('download')) {
            $asset = Asset::getById($request->get('id'));
            $response = new BinaryFileResponse($asset->getFileSystemPath());
            $response->headers->set('Content-Type', $asset->getMimetype());
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $asset->getFilename());
            return $response;
        } else {
            header('HTTP/1.0 403 Forbidden');
            echo 'Forbidden!';
        }
    }

    /**
    * @param Request $request
    * @Route("assets/filter-for-tags")
    * @Route("assets/filter-for-tag/")
    */
    public function filterForTagsAction(Request $request)
    {
        /* @var \Pimcore\Config\Config $aWebsiteConfig */
        $oWebsiteConfig = Config::getWebsiteConfig();

        /* @var \Pimcore\Model\Document\Page $oBamGuidelinesRoot */
        $iBamRootTag = intval($oWebsiteConfig->get('bam.root.tag'));

        $tagList = new \Pimcore\Model\Element\Tag\Listing();

        $aIN = explode(",", $request->get('tags'));
        foreach ($aIN as $iIN => $sIN) {
            $aIN[$iIN] = "'$sIN'";
        }
        $sIN = implode(",", $aIN);
        $tagList->setCondition("name IN (" . $sIN . ")");
//        $tagList->setCondition("idPath LIKE ?", ['/' . $iBamRootTag . '/%']);

        $this->view->aResults = $tagList->loadIdList();
//        $this->view->aResults = $tagList->load();

//        $listing = new Asset\Listing();

//        // get tags IDs to filter for - e.g. from request param
//        $values = $request->get('tags');
//        $values = is_array($values) ? $values : explode(',', $values);
//
//        if($values)
//        {
//            $conditionParts = [];
//            foreach ($values as $tagId) {
//
//                //decide if child tags should be considered or not
//                if ($request->get("considerChildTags") == "true") {
//                    $tag = \Pimcore\Model\Element\Tag::getById($tagId);
//                    if ($tag) {
//                        //get ID path of tag or filtering the child tags
//                        $tagPath = $tag->getFullIdPath();
//
//                        $conditionParts[] = "id IN (
//                            SELECT cId FROM tags_assignment INNER JOIN tags ON tags.id = tags_assignment.tagid
//                            WHERE
//                                ctype = 'asset' AND
//                                (id = " . intval($tagId) . " OR idPath LIKE " . $listing->quote($tagPath . "%") . ")
//                        )";
//                    }
//                } else {
//                    $conditionParts[] = "id IN (
//                        SELECT cId FROM tags_assignment WHERE ctype = 'asset' AND tagid = " . intval($tagId) .
//                        ")";
//                }
//            }
//
//
//            if (count($conditionParts) > 0) {
//                $condition = implode(" AND ", $conditionParts);
//                $listing->addConditionParam($condition);
//            }
//        }
//        $this->view->aResults = $listing->load();
    }

    /**
     * This is used for user-permissions, pass a permission type (eg. list, view, save) an you know if the current user is allowed to perform the requested action
     *
     * @param string $sType
     *
     * @return bool
     */
    public function isAllowed($sType = 'download')
    {
        /** @var \AppBundle\Model\DataObject\User $oUser */
        $oUser = $this->getUser();

        switch (true) {
            case $oUser instanceof \KITT3N\Pimcore\RestrictionsBundle\Model\DataObject\User:

                /* @var array|null $xRestrictions */
                $xRestrictions = $oUser->getGroup()->getAssetRestrictions();
                $aRestrictions = $xRestrictions === null ? [] : $xRestrictions;
                if ( ! in_array($sType, $aRestrictions)) {
                    return true;
                }

                break;
        }

        return false;
    }

}
