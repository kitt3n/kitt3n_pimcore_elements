<?php

namespace KITT3N\Pimcore\ElementsBundle\Controller;

use Pimcore\Controller\FrontendController;
use Pimcore\Model\Asset;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContentController extends FrontendController
{

    public function iconsAction(Request $request)
    {
        $this->view->eps = [];
        $this->view->png = [];
        $this->view->svg = [];

        if ('asset' === $request->get('type')) {
            $asset = Asset::getById($request->get('id'));
            if ('folder' === $asset->getType()) {

                $aChildren = $asset->getChildren();
                if (count($aChildren) === 3) {

                    switch (true) {
                        case 'folder' === $aChildren[0]->getType() and $aChildren[0]->getFilename() === "EPS":
                            $this->view->eps = $aChildren[0]->getChildren();
                            break;
                    }

                    switch (true) {
                        case 'folder' === $aChildren[1]->getType() and $aChildren[1]->getFilename() === "PNG":
                            $this->view->png = $aChildren[1]->getChildren();
                            break;
                    }

                    switch (true) {
                        case 'folder' === $aChildren[2]->getType() and $aChildren[2]->getFilename() === "SVG":
                            $this->view->svg = $aChildren[2]->getChildren();
                        break;
                    }
                }
                $this->view->assets = $asset->getChildren();

                /* @var \Pimcore\Model\Asset\Image[] $x */
                $x = $asset->getChildren();
                $y = [];
                foreach ($x as $i) {
                    $y[] = $i->getFullPath();
                }
                $z = 1;
            }
        }
    }

    /**
     * @Route("/kitt3n_pimcore_elements/download-icons-action")
     */
    public function downloadIconsAction(Request $request)
    {
        return new Response('Hello world from kitt3n_pimcore_elements');
    }
}
