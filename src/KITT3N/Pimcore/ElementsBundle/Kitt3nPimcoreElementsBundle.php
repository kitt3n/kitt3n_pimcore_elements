<?php

namespace KITT3N\Pimcore\ElementsBundle;

use PackageVersions\Versions;
use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class Kitt3nPimcoreElementsBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/kitt3npimcoreelements/js/pimcore/startup.js'
        ];
    }

    /**
     * @inheritDoc
     */
    public function getVersion()
    {
        return Versions::getVersion('kitt3n/pimcore-elements');
    }
}
