CKEDITOR.editorConfig = function( config ) {
    config.toolbar = [
        {
            name: "styles",
            items: ["Format"]
        },
        {
            name: "basicstyles",
            items: ["Bold", "Italic", "Strike", "RemoveFormat"]
        },
        {
            name: "paragraph",
            items: ["NumberedList", "BulletedList", "Blockquote"]
        },
        "/",
        {
            name: "clipboard",
            items: ["Cut", "Copy", "Paste", "PasteFromWord", "Undo", "Redo"]
        },
        {
            name: "links",
            items: ["Link", "Unlink"]
        }
    ];
    config.format_tags = 'p;h1;h2;h3;h4;h5;h6';
};