pimcore.registerNS("pimcore.plugin.Kitt3nPimcoreElementsBundle");

pimcore.plugin.Kitt3nPimcoreElementsBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.Kitt3nPimcoreElementsBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("Kitt3nPimcoreElementsBundle ready!");
    }
});

var Kitt3nPimcoreElementsBundlePlugin = new pimcore.plugin.Kitt3nPimcoreElementsBundle();
