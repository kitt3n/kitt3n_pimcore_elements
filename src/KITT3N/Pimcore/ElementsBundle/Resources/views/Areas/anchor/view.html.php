<?php

/* Shared variables / fields */
echo $this->template("@Kitt3nPimcoreElementsBundle/Resources/views/Includes/Edit/Elements/Columns/variables.html.php");
$iUniqueIdentifier = $this->input("unique_identifier")->getData();

?>

<div id="element--<?= $iUniqueIdentifier ?>"
     class="element element--anchor<?php if ($this->editmode): ?> edit<?php endif; ?>">

    <?php if ($this->editmode): ?>
        <hr />
        <span>&#9875;</span>
        <?= $this->input("anchor_nav_title", [
            "placeholder" =>
                "Navigation title"
        ]); ?>
        <?= $this->input("anchor_href", [
            "placeholder" =>
                "Unique anchor href"
        ]); ?>
        <hr />
    <?php endif; ?>

    <a class="anchor"
       href="javascript:;"
       name="<?= $this->input("anchor_href")->getData(); ?>"
       id="<?= $this->input("anchor_href")->getData(); ?>"><?= $this->input("anchor_nav_title")->getData(); ?></a>

</div>
