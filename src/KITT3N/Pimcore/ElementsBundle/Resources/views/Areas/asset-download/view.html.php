<?php

use Pimcore\Model\Asset;
use Pimcore\Model\Document\Tag\Area\Info;

/* @var Pimcore\Model\Document\Tag\Area\Info $info */
$aGlobalParams = $info->getParams();

/* @var Pimcore\Model\Document\Tag\Area\Info $brick */
$cTag = $brick->getTag();

/* Unique identifier */
$iCurrentIndex = $cTag->currentIndex['key'];

/*
 * $cTag->currentIndex['key'] is available if element is an "areablock"
 * If element is an "area" use $this->suffix cause $cTag->currentIndex['key'] is null here
 */
$sSuffix = ($iCurrentIndex === null ? '': '_' . $iCurrentIndex) . (isset($this->suffix) ? '_' . $this->suffix : '');

$sUid = $aGlobalParams["uid"] . '_' . $aGlobalParams["col"] . $sSuffix;

?>

<div id="element--<?= $sUid ?>"
     class="element element--asset-download<?php if ($this->editmode): ?> edit<?php endif; ?>">

    <?php if ($this->editmode): ?>
        <?= $this->relations("assetDownload", [
            "types" => ["asset"],
            "subtypes" => [
                "asset" => ["document", "text", "video", "image"],
            ],
        ]); ?>
    <?php else: ?>
        <ul>
            <?php foreach($this->relations("assetDownload") as $oElement):
                /** @var \Pimcore\Model\Element\ElementInterface $oElement */
                /** @var \Pimcore\Model\Asset $oAsset */
                $oAsset = Asset::getById($oElement->getId());
                ?>
                <?php if ( ! in_array("download", $aGlobalParams["aBamParams"]["aRestrictions"]["aAssetRestrictions"])): ?>
                    <li class="clickable" onclick="javascript:location.href='/kitt3npimcoreelements/simple-file-downloader/asset/<?= $oAsset->getId(); ?>';">
                        <strong><?= $oAsset->getFilename(); ?></strong>
                        <span class="size"><?= $oAsset->getFileSize(true, 2); ?></span>
                        <a class="download"
                           href="/kitt3npimcoreelements/simple-file-downloader/asset/<?= $oAsset->getId(); ?>">dl</a>
                    </li>
                <?php else: ?>
                    <li>
                        <strong><?= $oAsset->getFilename(); ?></strong>
                        <span class="size"><?= $oAsset->getFileSize(true, 2); ?></span>
                    </li>
                <?php endif; ?>

            <?php endforeach; ?>
        </ul>

    <?php endif; ?>


</div>
