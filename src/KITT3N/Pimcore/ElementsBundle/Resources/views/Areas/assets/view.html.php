<?php

use Pimcore\Model\Document\Tag\Area\Info;

/* @var Pimcore\Model\Document\Tag\Area\Info $info */
$aGlobalParams = $info->getParams();

/* @var Pimcore\Model\Document\Tag\Area\Info $brick */
$cTag = $brick->getTag();

/* Unique identifier */
$iCurrentIndex = $cTag->currentIndex['key'];

/*
 * $cTag->currentIndex['key'] is available if element is an "areablock"
 * If element is an "area" use $this->suffix cause $cTag->currentIndex['key'] is null here
 */
$sSuffix = ($iCurrentIndex === null ? '': '_' . $iCurrentIndex) . (isset($this->suffix) ? '_' . $this->suffix : '');

$sUid = $aGlobalParams["uid"] . '_' . $aGlobalParams["col"] . $sSuffix;

?>

<div id="element--<?= $sUid ?>"
     class="element element--assets<?php if ($this->editmode): ?> edit<?php endif; ?>">

<?= $this->renderlet("assets", [
    "type" => "asset",
    "subtype" => "folder",
    "controller" => "asset",
    "action" => "simpleFileBrowser",
    "bundle" => "Kitt3nPimcoreElementsBundle",
    "title" => "Drag a folder here",
    "height" => "85px",

    // Custom parameters
    "editmode" => $this->editmode,
    "aAllParams" => $this->getAllParams(),
    "sUid" => $sUid,
]); ?>

</div>
