<?php

use Pimcore\Model\Document\Tag\Area\Info;

##
## Base variables
##

/* @var Pimcore\Model\Document\Tag\Area\Info $info */
$aGlobalParams = $info->getParams();

/* @var Pimcore\Model\Document\Tag\Area\Info $brick */
$cTag = $brick->getTag();

/* Current element index */
$iCurrentIndex = $cTag->currentIndex['key'];

/*
 * $cTag->currentIndex['key'] is available if element is an "areablock"
 * If element is an "area" use $this->suffix cause $cTag->currentIndex['key'] is null here
 */
$sSuffix = ($iCurrentIndex === null ? '': '_' . $iCurrentIndex) . (isset($this->suffix) ? '_' . $this->suffix : '');

$sUid = $aGlobalParams["uid"] . '_' . $aGlobalParams["col"] . $sSuffix;

?>

<?php

##
## Pimcore fields
## with default values
##

if ($this->input("color_hex")->isEmpty()) {
    $this->input("color_hex")->setDataFromResource("#000000");
}
if ($this->input("color_hex8")->isEmpty()) {
    $this->input("color_hex8")->setDataFromResource("#000000ff");
}
if ($this->input("color_rgba")->isEmpty()) {
    $this->input("color_rgba")->setDataFromResource("0/0/0/1");
}
if ($this->input("color_cmyk")->isEmpty()) {
    $this->input("color_cmyk")->setDataFromResource("0/0/0/1");
}

?>

<div id="element--<?= $sUid ?>"
     class="element element--color<?php if($this->editmode): ?> edit<?php endif; ?>">

    <?php if ($this->editmode): ?>

        <div id="app--<?= $sUid; ?>"
             class="app"
             style="background: <?= $this->input("color_hex8")->getData(); ?>;">
            <sketch-picker v-model="colors" @input="onchange($event)"/>
        </div>

        <div id="edit--<?= $sUid; ?>">

            <!-- !!! Place pimcore fields outside of app container
            because the color-picker component overrides the whole container content !!! -->

            <div class="edit color">
                <div class="display_none_">
                    <h2><?= $this->input("color", ["placeholder" => "Primärfarbe"]); ?></h2>
                </div>
            </div>

            <div class="edit color_cmyk">
                <div class="display_none">
                    <?= $this->input("color_cmyk"); ?>
                </div>
            </div>

            <div class="edit color_hex">
                <div class="display_none">
                    <?= $this->input("color_hex"); ?>
                </div>
            </div>

            <div class="edit color_hex8">
                <div class="display_none">
                    <?= $this->input("color_hex8"); ?>
                </div>
            </div>

            <div class="edit color_rgba">
                <div class="display_none">
                    <?= $this->input("color_rgba"); ?>
                </div>
            </div>

        </div>

    <?php endif; ?>

        <div id="view--<?= $sUid; ?>">

            <?php if ( ! $this->editmode): ?>

            <div class="colorSquare"
                 id="colorSquare--<?= $sUid; ?>"
                 style="background: <?= $this->input("color_hex")->getData(); ?>;">
            </div>

            <h3><?= $this->input("color")->getData(); ?></h3>

            <?php endif; ?>

            <table>
                <tr>
                    <th><?= $this->translate("ElementsBundle__color_cmyk"); ?></th>
                    <td class="view color_cmyk">
                        <a id="color_cmyk--<?= $sUid; ?>"
                           class="copyToClipboard"
                           href="javascript:copyStringToClipboard (document.getElementById('color_cmyk--<?= $sUid; ?>').innerText);"
                           data-view-name="color_cmyk">
                            <?php if ($this->editmode): ?>
                                <?= $this->input("color_cmyk")->getData(); ?>
                            <?php else: ?>
                                <?php if ($this->input("color_cmyk_override")->isEmpty()): ?>
                                    <?= $this->input("color_cmyk")->getData(); ?>
                                <?php else: ?>
                                    <?= $this->input("color_cmyk_override")->getData(); ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </a>
                    </td>
                </tr>
                <?php if ($this->editmode): ?>
                    <tr>
                        <th></th>
                        <td class="view color_cmyk_override">
                            <?= $this->input("color_cmyk_override", ["placeholder" => "0/0/0/1"]); ?>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <th><?= $this->translate("ElementsBundle__color_rgba"); ?></th>
                    <td class="view color_rgba">
                        <a id="color_rgba--<?= $sUid; ?>"
                           class="copyToClipboard"
                           href="javascript:copyStringToClipboard (document.getElementById('color_rgba--<?= $sUid; ?>').innerText);"
                           data-view-name="color_rgba">
                            <?= $this->input("color_rgba")->getData(); ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <th><?= $this->translate("ElementsBundle__color_hex"); ?></th>
                    <td class="view color_hex">
                        <a id="color_hex--<?= $sUid; ?>"
                           class="copyToClipboard"
                           href="javascript:copyStringToClipboard (document.getElementById('color_hex--<?= $sUid; ?>').innerText);"
                           data-view-name="color_hex">
                            <?= $this->input("color_hex")->getData(); ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <th><?= $this->translate("ElementsBundle__color_hex8"); ?></th>
                    <td class="view color_hex8">
                        <a id="color_hex8--<?= $sUid; ?>"
                           class="copyToClipboard"
                           href="javascript:copyStringToClipboard (document.getElementById('color_hex8--<?= $sUid; ?>').innerText);"
                           data-view-name="color_hex8">
                            <?= $this->input("color_hex8")->getData(); ?>
                        </a>
                    </td>
                </tr>

                <?php

                $aEditColors = [
                    [
                        'ral',
                        '000'
                    ],
                    [
                        'hks',
                        'HKS 88'
                    ],
                    [
                        'pantone',
                        '000'
                    ],
                    [
                        'gray',
                        '0% K'
                    ],
                ];

                ?>

                <?php foreach ($aEditColors as $aEditColor): ?>

                    <?php if ($this->editmode): ?>

                        <tr>
                            <th><?= $this->translate("ElementsBundle__color_" . $aEditColor[0]) ?></th>
                            <td class="view color_<?= $aEditColor[0]; ?>">
                                <?= $this->input("color_" . $aEditColor[0], ["placeholder" => "$aEditColor[1]"]); ?>
                            </td>
                        </tr
                        >
                    <?php else: ?>

                        <?php if ( ! $this->input("color_" . $aEditColor[0])->isEmpty()): ?>

                            <tr>
                                <th><?= $this->translate("ElementsBundle__color_" . $aEditColor[0]) ?></th>
                                <td class="view color_<?= $aEditColor[0] ?>">
                                    <a id="color_<?= $aEditColor[0]; ?>--<?= $sUid; ?>"
                                       class="copyToClipboard"
                                       href="javascript:copyStringToClipboard (document.getElementById('color_<?= $aEditColor[0]; ?>--<?= $sUid; ?>').innerText);"
                                       data-view-name="color_<?= $aEditColor[0]; ?>">
                                        <?= $this->input("color_" . $aEditColor[0])->getData(); ?>
                                    </a>
                                </td>
                            </tr>

                        <?php endif; ?>

                    <?php endif; ?>

                <?php endforeach; ?>

            </table>

        </div>

</div>

<?php if ($this->editmode): ?>

<script>

    function rgb2cmyk (r,g,b) {
        var computedC = 0;
        var computedM = 0;
        var computedY = 0;
        var computedK = 0;

        //remove spaces from input RGB values, convert to int
        var r = parseInt( (''+r).replace(/\s/g,''),10 );
        var g = parseInt( (''+g).replace(/\s/g,''),10 );
        var b = parseInt( (''+b).replace(/\s/g,''),10 );

        if ( r==null || g==null || b==null ||
            isNaN(r) || isNaN(g)|| isNaN(b) )
        {
            alert ('Please enter numeric RGB values!');
            return;
        }
        if (r<0 || g<0 || b<0 || r>255 || g>255 || b>255) {
            alert ('RGB values must be in the range 0 to 255.');
            return;
        }

        // BLACK
        if (r==0 && g==0 && b==0) {
            computedK = 1;
            return [0,0,0,1];
        }

        computedC = 1 - (r/255);
        computedM = 1 - (g/255);
        computedY = 1 - (b/255);

        var minCMY = Math.min(computedC,
            Math.min(computedM,computedY));
        computedC = (computedC - minCMY) / (1 - minCMY) ;
        computedM = (computedM - minCMY) / (1 - minCMY) ;
        computedY = (computedY - minCMY) / (1 - minCMY) ;
        computedK = minCMY;

        return [
            Math.round(computedC*100),
            Math.round(computedM*100),
            Math.round(computedY*100),
            Math.round(computedK*100)
        ];
    }

    var colorPicker__<?= $sUid; ?> = new Vue({
        el: '#app--<?= $sUid; ?>',
        components: {
            'sketch-picker': VueColor.Sketch
        },
        data: {
            colors: "<?= $this->input('color_hex8')->getData(); ?>",
        },
        methods: {
            onchange: function (event) {

                var oApp = this.$el;
                var oEdit = oApp.nextElementSibling;
                var oView = oEdit.nextElementSibling;

                var oColorCmyk = oEdit.querySelector('[data-real-name="color_cmyk"]');
                var oColorHex = oEdit.querySelector('[data-real-name="color_hex"]');
                var oColorHex8 = oEdit.querySelector('[data-real-name="color_hex8"]');
                var oColorRgba = oEdit.querySelector('[data-real-name="color_rgba"]');

                var oColorCmykView = oView.querySelector('[data-view-name="color_cmyk"]');
                var oColorHexView = oView.querySelector('[data-view-name="color_hex"]');
                var oColorHex8View = oView.querySelector('[data-view-name="color_hex8"]');
                var oColorRgbaView = oView.querySelector('[data-view-name="color_rgba"]');

                oApp.setAttribute('style', 'background: ' + event.hex + ';'); console.warn(event.hex);

                oColorHex.innerHTML = event.hex;
                oColorHexView.innerHTML = event.hex;

                oColorHex8.innerHTML = event.hex8;
                oColorHex8View.innerHTML = event.hex8;

                oColorRgba.innerHTML = event.rgba.r + '/' + event.rgba.g + '/' + event.rgba.b + '/' + (+(Math.round(event.rgba.a + "e+2")  + "e-2"));
                oColorRgbaView.innerHTML = event.rgba.r + '/' + event.rgba.g + '/' + event.rgba.b + '/' + (+(Math.round(event.rgba.a + "e+2")  + "e-2"));

                var aCmyk = rgb2cmyk (event.rgba.r, event.rgba.g, event.rgba.b);
                oColorCmyk.innerHTML = aCmyk[0] + '/' + aCmyk[1] + '/' + aCmyk[2] + '/' + aCmyk[3];
                oColorCmykView.innerHTML = aCmyk[0] + '/' + aCmyk[1] + '/' + aCmyk[2] + '/' + aCmyk[3];

            }
        },
    });

</script>

<?php endif; ?>