<?php

use Pimcore\Model\Document\Tag\Area\Info;

/* @var Pimcore\Model\Document\Tag\Area\Info $info */
$aGlobalParams = $info->getParams();

/* @var Pimcore\Model\Document\Tag\Area\Info $brick */
$cTag = $brick->getTag();

/* Unique identifier */
$iCurrentIndex = $cTag->currentIndex['key'];

/*
 * $cTag->currentIndex['key'] is available if element is an "areablock"
 * If element is an "area" use $this->suffix cause $cTag->currentIndex['key'] is null here
 */
$sSuffix = ($iCurrentIndex === null ? '': '_' . $iCurrentIndex) . (isset($this->suffix) ? '_' . $this->suffix : '');

$sUid = $aGlobalParams["uid"] . '_' . $aGlobalParams["col"] . $sSuffix;

?>

<div id="element--<?= $sUid ?>"
     class="element element--icons<?php if ($this->editmode): ?> edit<?php endif; ?>">

<?= $this->renderlet("icons", [
    "type" => "asset",
    "subtype" => "folder",
    "controller" => "content",
    "action" => "icons",
    "bundle" => "Kitt3nPimcoreElementsBundle",
    "title" => "Drag an icon folder here",
    "height" => "auto",
    "editmode" => $this->editmode,
    "aAllParams" => $this->getAllParams(),
]); ?>

</div>
