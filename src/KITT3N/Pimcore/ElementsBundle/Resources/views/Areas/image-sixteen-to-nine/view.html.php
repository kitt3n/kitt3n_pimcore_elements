<?php if ($this->editmode): ?>

    <?= $this->image(
        "image",
        [
            "thumbnail" => [
                "width" => $this->width,
                //"height" => $this->height,
                "interlace" => true,
                "quality" => 90,
            ],
            "disableInlineUpload" => true,
            "cacheBuster" => false,
//            "ratioX" => 16,
//            "ratioY" => 9,

        ]
    ); ?>

<?php else: ?>

    <?php if ($this->selector === "source"): ?>

        <source
            media="<?= $this->media; ?>"
            data-srcset="<?= $this->image("image")->getThumbnail([
                "width" => $this->width,
                //"height" => $this->height,
                "interlace" => true,
                "quality" => 90,
            ]); ?>">

    <?php else: ?>

        <img
            class="lazy"
            media="<?= $this->media; ?>"
            data-src="<?= $this->image("image")->getThumbnail([
                "width" => $this->width,
               // "height" => $this->height,
                "interlace" => true,
                "quality" => 90,
            ]); ?>">

        <noscript>
            <img
                class="lazy"
                media="<?= $this->media; ?>"
                src="<?= $this->image("image")->getThumbnail([
                    "width" => $this->width,
                   // "height" => $this->height,
                    "interlace" => true,
                    "quality" => 90,
                ]); ?>">
        </noscript>

    <?php endif; ?>

<?php endif; ?>

