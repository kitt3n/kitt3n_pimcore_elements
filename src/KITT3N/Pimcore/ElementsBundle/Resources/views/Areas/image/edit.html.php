<?= $this->image(
    "image",
    [
        "thumbnail" => [
            "width" => $this->width,
            "interlace" => true,
            "quality" => 90,
        ],
        "disableInlineUpload" => true,
        "cacheBuster" => false,
    ]
); ?>


