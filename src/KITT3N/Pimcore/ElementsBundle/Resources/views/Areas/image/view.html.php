<?php

use Pimcore\Model\Document\Tag\Area\Info;

/* @var Pimcore\Model\Document\Tag\Area\Info $info */
$aGlobalParams = $info->getParams();

/* @var Pimcore\Model\Document\Tag\Area\Info $brick */
$cTag = $brick->getTag();

/* Unique identifier */
$iCurrentIndex = $cTag->currentIndex['key'];

/*
 * $cTag->currentIndex['key'] is available if element is an "areablock"
 * If element is an "area" use $this->suffix cause $cTag->currentIndex['key'] is null here
 */
$sSuffix = ($iCurrentIndex === null ? '': '_' . $iCurrentIndex) . (isset($this->suffix) ? '_' . $this->suffix : '');

$sUid = $aGlobalParams["uid"] . '_' . $aGlobalParams["col"] . $sSuffix;

$sClassNogo = ' nogo';
if ($this->input("nogo")->getData() === '') {
    $sClassNogo = '';
}

?>

<div id="element--<?= $sUid ?>"
     class="element element--image<?= $sClassNogo; ?><?php if ($this->editmode): ?> edit<?php endif; ?>">

    <?php if ($this->editmode): ?>

    <div id="app--<?= $sUid; ?>"
         class="app">

        <div class="edit controlsPlaceholder">
        </div>

        <div class="edit controls">

            <div class="edit nogo">
                <div class="pimcore_tag_checkbox">
                    <input name="app__checkbox_nogo"
                           @change="checkbox($event)"
                           data-target="nogo"
                           value="1"
                           data-value="nogo"
                           type="checkbox"
                           v-model="nogo" />
                </div>
                <div class="display_none">
                    <?= $this->input("nogo"); ?>
                </div>
            </div>

        </div>

    </div>

    <?php endif; ?>

    <?= $this->image(
        "image",
        [
            "thumbnail" => [
                "width" => 1920,
                "interlace" => true,
                "quality" => 100,
            ],
            "disableInlineUpload" => true,
            "cacheBuster" => false,
            "reload" => true,
        ]
    ); ?>

</div>

<?php if ($this->editmode): ?>

    <script>

        var vueapp_<?= $sUid; ?> = new Vue({
            el: "#app--<?= $sUid; ?>",
            data: {
                nogo: "<?= $this->input('nogo')->getData(); ?>"
            },
            methods: {
                checkbox: function(event) {

                    var oEventSource = event.target;
                    var sDataEventTarget = oEventSource.getAttribute('data-target');

                    var oApp = this.$el;

                    //
                    // Get pimcore input element inside app div by data-real-name attribute
                    // and set new value
                    //
                    var oEventTarget = oApp.querySelector('[data-real-name="' + sDataEventTarget + '"]');

                    var sValue = '';
                    switch (true) {
                        case event.target.checked === true:
                            sValue = 1;
                            break;
                    }

                    oEventTarget.innerHTML = sValue;

                    //
                    // Add/Remove nogo class to #edit... div when clicking the checkbox
                    //
                    var oElement = document.getElementById("element--<?= $sUid ?>");
                    var sCurrentClasses = oElement.getAttribute('class');
                    var sDataValue = oEventSource.getAttribute('data-value');
                    sCurrentClasses = sCurrentClasses.replace(' ' + sDataValue, '');
                    sCurrentClasses = sCurrentClasses.replace(sDataValue, '');
                    if (sValue !== '') {
                        sCurrentClasses += ' ' + sDataValue;
                    }
                    oElement.setAttribute('class' ,sCurrentClasses);

                }

            }
        })

    </script>

<?php endif; ?>
