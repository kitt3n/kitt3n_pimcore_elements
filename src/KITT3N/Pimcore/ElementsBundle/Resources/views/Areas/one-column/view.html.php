<?php

/* @var Pimcore\Model\Document\Tag\Area\Info $info */
$aGlobalParams = $info->getParams();

/* Shared variables / fields */
echo $this->template("@Kitt3nPimcoreElementsBundle/Resources/views/Includes/Edit/Elements/Columns/variables.html.php");
$iUniqueIdentifier = $this->input("unique_identifier")->getData();

/* @var string $sGridContainerClasses */
$sGridContainerClasses = '';

/*
* Column(s) default value
*/
if ($this->input("column_sizes")->isEmpty()) {
    $this->input("column_sizes")->setDataFromResource("c12");
}

/* @var string $sGridContainerClasses */
$sGridContainerClasses .= $this->input("column_sizes")->getData();

use Pimcore\Model\Document\Tag\Area\Info; ?>

<div id="wrapper--<?= $iUniqueIdentifier ?>"
     class="wrapper<?= $this->input("wrapper_classes")->getData(); ?><?php if($this->editmode): ?> edit<?php endif; ?>"
     data-base-classes="wrapper<?php if($this->editmode): ?> edit<?php endif; ?>">

    <?php if($this->editmode): ?>

        <div id="app--<?= $iUniqueIdentifier; ?>"
             class="app">

            <div class="edit controlsPlaceholder">
            </div>

            <div class="edit controls">

                <!-- Column(s) are different in each x-column(s) area -->
                <select name="app__select_column_sizes"
                        @change="onchange($event)"
                        data-target="column_sizes"
                        data-target-element="grid-container"
                        data-options="c12,c10,c8"
                        v-model="columnSizes"
                        title="Column(s)">
                    <option value="c12">12</option>
                    <option value="c10">10</option>
                    <option value="c8">8</option>
                </select>

                <!-- Shared control fields -->
                <?= $this->template("@Kitt3nPimcoreElementsBundle/Resources/views/Includes/Edit/App/Controls/shared.html.php"); ?>

            </div>

            <!-- Other shared fields -->
            <?= $this->template("@Kitt3nPimcoreElementsBundle/Resources/views/Includes/Edit/App/shared.html.php"); ?>

        </div>

    <?php endif; ?>

    <div id="grid-container--<?= $iUniqueIdentifier; ?>"
         class="grid-container columns <?= $sGridContainerClasses; ?>">
        <div class="column">
            <?= $this->template("@Kitt3nPimcoreElementsBundle/Resources/views/Includes/Edit/Elements/Columns/elements_x.html.php",
                    [
                        "aBamParams" => $aGlobalParams["aBamParams"],
                        "col" => 0,
                        "uid" => $iUniqueIdentifier,
                    ]
            ); ?>
        </div>
    </div>
</div>

<?= $this->template("@Kitt3nPimcoreElementsBundle/Resources/views/Includes/Edit/App/script.html.php", ["cIndex" => $iUniqueIdentifier]); ?>
