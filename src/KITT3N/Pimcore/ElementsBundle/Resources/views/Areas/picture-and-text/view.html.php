<?php

use Pimcore\Model\Document\Tag\Area\Info;

/* @var Pimcore\Model\Document\Tag\Area\Info $info */
$aGlobalParams = $info->getParams();

/* @var Pimcore\Model\Document\Tag\Area\Info $brick */
$cTag = $brick->getTag();

/* Unique identifier */
$iCurrentIndex = $cTag->currentIndex['key'];

/*
 * $cTag->currentIndex['key'] is available if element is an "areablock"
 * If element is an "area" use $this->suffix cause $cTag->currentIndex['key'] is null here
 */
$sSuffix = ($iCurrentIndex === null ? '': '_' . $iCurrentIndex) . (isset($this->suffix) ? '_' . $this->suffix : '');

$sUid = $aGlobalParams["uid"] . '_' . $aGlobalParams["col"] . $sSuffix;

?>

<div id="element--<?= $sUid ?>"
     class="element element--picture-and-text<?php if ($this->editmode): ?> edit<?php endif; ?>">

    <?= $this->area('picture', [
            'type' => 'picture',
            "params" => [
                /*
                 * Element is an area here so pass an unique suffix as param to the element!
                 */
                'picture' => [
                    "uid" => $this->uid, // pass uid to element in element
                    "col" => $this->col, // pass col to element in element
                    "suffix" => $iCurrentIndex . '_' . 1, // pass suffix to element in element
                ],
            ],
        ]
    ); ?>

    <?= $this->area('wysiwyg', [
            'type' => 'w-y-s-i-w-y-g',
            "params" => [
                /*
                 * Element is an area here so pass an unique suffix as param to the element!
                 */
                'w-y-s-i-w-y-g' => [
                    "uid" => $this->uid,// pass uid to element in element
                    "col" => $this->col, // pass col to element in element
                    "suffix" => $iCurrentIndex . '_' . 2, // pass suffix to element in element
                ],
            ],
        ]
    ); ?>

</div>
