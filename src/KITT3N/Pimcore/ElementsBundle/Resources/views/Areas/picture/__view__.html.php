<?php

use Pimcore\Model\Document\Tag\Area\Info;

$aCount = [
    $this->areablock('image_0')->getCount(),
    $this->areablock('image_1')->getCount(),
    $this->areablock('image_2')->getCount(),
];

$aR = [
    "image-sixteen-to-nine" => [
        "aR--0-16_9",
        "aR--1-16_9",
        "aR--2-16_9",
    ]
];

?>

<?php if ($this->editmode): ?>

    <strong>Smartphone (< 768px)</strong>
    <?= $this->areablock('image_2', [
        'allowed' => [
            'image',
            'image-sixteen-to-nine',
        ],
        'sorting' => [
            'image',
            'image-sixteen-to-nine',
        ],
        'limit' => 1,
        "params" => [
            'image' => [
                "width" => 768,
            ],
            'image-sixteen-to-nine' => [
                "width" => 768,
                "height" => 432
            ],
        ]
    ]); ?>

    <strong>Tablet (>= 768 < 992px)</strong>
    <?= $this->areablock('image_1', [
        'allowed' => [
            'image',
            'image-sixteen-to-nine',
        ],
        'sorting' => [
            'image',
            'image-sixteen-to-nine',
        ],
        'limit' => 1,
        "params" => [
            'image' => [
                "width" => 992,
            ],
            'image-sixteen-to-nine' => [ // some additional parameters / configuration for the brick type "iframe"
                "width" => 992,
                "height" => 558
            ],
        ]
    ]); ?>

    <strong>Desktop (>= 1200px)</strong>
    <?= $this->areablock('image_0', [
        'allowed' => [
            'image',
            'image-sixteen-to-nine',
        ],
        'sorting' => [
            'image',
            'image-sixteen-to-nine',
        ],
        'limit' => 1,
        "params" => [
            'image' => [
                "width" => 1920,
            ],
            'image-sixteen-to-nine' => [ // some additional parameters / configuration for the brick type "iframe"
                "width" => 1920,
                "height" => 1080,
            ],
        ]
    ]); ?>

<?php else: ?>

    <?php if ($aCount[0] > 0 or $aCount[1] > 0 or $aCount[2] > 0): ?>

    <?php

        /* @var Pimcore\Model\Document\Tag\Area\Info $brick */
        $cTag = $brick->getTag();

        /* Unique identifier */
        $iCurrentIndex = $cTag->currentIndex['key'];

        $aRClasses = [];

        $aProcessedImageCsss = [];

        /*
         * Fallback for
         * - IEs 9 and lower
         * - Edge 12-15
         */
        $aProcessedImageFallbackCsss = [];

        /*
         * Fallback for
         * - IEs 10 and 11
         */
        $aProcessedImageIE10PlusFallbackCsss = [];

        $aProcessedImageFallbackCsss[] =
            '#garfield-' . $iCurrentIndex . '.tx-kitt3n-image > .aR > img { display: none;}';

        $aProcessedImageIE10PlusFallbackCsss[] =
            '@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {' .
            '#garfield-' . $iCurrentIndex . '.tx-kitt3n-image > .aR > img { display: none;}}';

    if ($aCount[2] > 0) {
        $aData2 = $this->areablock('image_2')->getData();
        $aRClasses[] = $aR[$aData2[0]['type']][2];

        $aProcessedImageCsss[] =
            '#garfield-' . $iCurrentIndex . '.tx-kitt3n-image > .aR > img { object-position: 50% 50%; }';

    }


    $aDataM = $this->areablock('image_m')->getData();
    $aDataL = $this->areablock('image_l')->getData();

    ?>

    <style><?= implode(" ", $aProcessedImageCsss); ?></style>
    <figure class="tx-kitt3n-image" id="garfield-<?= $iCurrentIndex ?>">
        <picture class="aR <?= implode(" ", $aRClasses); ?>">

        <?php if ($aCount[0] > 0): ?>

            <?= $this->areablock('image_0', [
                'allowed' => [
                    'image-sixteen-to-nine',
                ],
                'sorting' => [
                    'image-sixteen-to-nine',
                ],
                "limit" => 1,
                "params" => [
                    "image-sixteen-to-nine" => [ // some additional parameters / configuration for the brick
                        "width" => 1920,
                        "height" => 1080,
                        "selector" => "source",
                        "media" => "(min-width: 1200px)",
                    ],
                ]
            ]); ?>

        <?php endif; ?>

        <?php if ($aCount[1] > 0): ?>

            <?= $this->areablock('image_1', [
                'allowed' => [
                    'image-sixteen-to-nine',
                ],
                'sorting' => [
                    'image-sixteen-to-nine',
                ],
                "limit" => 1,
                "params" => [
                    "image-sixteen-to-nine" => [ // some additional parameters / configuration for the brick
                        "width" => 1200,
                        "height" => 675,
                        "selector" => "source",
                        "media" => "(min-width: 992px)",
                    ],
                ]
            ]); ?>

        <?php endif; ?>

        <?php if ($aCount[2] > 0): ?>

            <?= $this->areablock('image_2', [
                'allowed' => [
                    'image-sixteen-to-nine',
                ],
                'sorting' => [
                    'image-sixteen-to-nine',
                ],
                "limit" => 1,
                "params" => [
                    "image-sixteen-to-nine" => [ // some additional parameters / configuration for the brick
                        "width" => 992,
                        "height" => 558,
                        "selector" => "img",
                        "media" => "",
                    ],
                ]
            ]); ?>

        <?php endif; ?>

        </picture>
    </figure>

    <?php endif; ?>

<?php endif; ?>