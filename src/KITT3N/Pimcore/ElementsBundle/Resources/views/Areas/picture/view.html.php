<?php

use Pimcore\Model\Document\Tag\Area\Info;

/* @var Pimcore\Model\Document\Tag\Area\Info $info */
$aGlobalParams = $info->getParams();

/* @var Pimcore\Model\Document\Tag\Area\Info $brick */
$cTag = $brick->getTag();

/* Unique identifier */
$iCurrentIndex = $cTag->currentIndex['key'];

/*
 * $cTag->currentIndex['key'] is available if element is an "areablock"
 * If element is an "area" use $this->suffix cause $cTag->currentIndex['key'] is null here
 */
$sSuffix = ($iCurrentIndex === null ? '': '_' . $iCurrentIndex) . (isset($this->suffix) ? '_' . $this->suffix : '');

$sUid = $aGlobalParams["uid"] . '_' . $aGlobalParams["col"] . $sSuffix;

if ($this->input("ratio_0")->isEmpty()) {
    $this->input("ratio_0")->setDataFromResource("aR--0-16_9");
}
if ($this->input("ratio_1")->isEmpty()) {
    $this->input("ratio_1")->setDataFromResource("aR--1-16_9");
}
if ($this->input("ratio_2")->isEmpty()) {
    $this->input("ratio_2")->setDataFromResource("aR--2-4_3");
}

if (!$this->image("image")->isEmpty()) {

    $aMarkers = $this->image("image")->getMarker();

    switch (true) {
        case empty($aMarkers):

            $sPosition = "50% 50%";
            $sLeft = "50%";
            $sTop = "50%";

            break;
        default:

            $sPosition = $aMarkers[0]['left'] . "% " . $aMarkers[0]['top'] . "%";
            $sLeft = $aMarkers[0]['left'] . "%";
            $sTop = $aMarkers[0]['top'] . "%";

    }

}

$sClassNogo = ' nogo';
if ($this->input("nogo")->getData() === '') {
    $sClassNogo = '';
}

?>

<div id="element--<?= $sUid ?>"
     class="element element--picture<?= $sClassNogo; ?><?php if ($this->editmode): ?> edit<?php endif; ?>">

    <?php if ($this->editmode): ?>

        <div id="app--<?= $sUid; ?>"
             class="app">

            <div class="edit controlsPlaceholder">
            </div>

            <div class="edit controls">

                <!-- Column(s) are different in each x-column(s) area -->
                <select name="app__select_ratio_0"
                        @change="onchange($event)"
                        data-target="ratio_0"
                        data-options="aR--0-1_1,aR--0-4_3,aR--0-3_2,aR--0-2_1,aR--0-16_9,aR--0-21_9,aR--0-21_5,aR--0-2_3,aR--0-3_4"
                        v-model="ratio0"
                        title="Ratio (Desktop)">
                    <option value="aR--0-1_1">1:1</option>
                    <option value="aR--0-4_3">4:3</option>
                    <option value="aR--0-3_2">3:2</option>
                    <option value="aR--0-2_1">2:1</option>
                    <option value="aR--0-16_9">16:9</option>
                    <option value="aR--0-21_9">21:9</option>
                    <option value="aR--0-21_5">21:5</option>
                    <option value="aR--0-2_3">2:3</option>
                    <option value="aR--0-3_4">3:4</option>
                </select>

                <div class="edit ratio_0">
                    <div class="display_none">
                        <?= $this->input("ratio_0"); ?>
                    </div>
                </div>

                <!-- Column(s) are different in each x-column(s) area -->
                <select name="app__select_ratio_1"
                        @change="onchange($event)"
                        data-target="ratio_1"
                        data-options="aR--1-1_1,aR--1-4_3,aR--1-3_2,aR--1-2_1,aR--1-16_9,aR--1-21_9,aR--1-21_5,aR--1-2_3,aR--1-3_4"
                        v-model="ratio1"
                        title="Ratio (Tablet)">
                    <option value="aR--1-1_1">1:1</option>
                    <option value="aR--1-4_3">4:3</option>
                    <option value="aR--1-3_2">3:2</option>
                    <option value="aR--1-2_1">2:1</option>
                    <option value="aR--1-16_9">16:9</option>
                    <option value="aR--1-21_9">21:9</option>
                    <option value="aR--1-21_5">21:5</option>
                    <option value="aR--1-2_3">2:3</option>
                    <option value="aR--1-3_4">3:4</option>
                </select>

                <div class="edit ratio_1">
                    <div class="display_none">
                        <?= $this->input("ratio_1"); ?>
                    </div>
                </div>

                <!-- Column(s) are different in each x-column(s) area -->
                <select name="app__select_ratio_2"
                        @change="onchange($event)"
                        data-target="ratio_2"
                        data-options="aR--2-1_1,aR--2-4_3,aR--2-3_2,aR--2-2_1,aR--2-16_9,aR--2-21_9,aR--2-21_5,aR--2-2_3,aR--2-3_4"
                        v-model="ratio2"
                        title="Ratio (Smartphone)">
                    <option value="aR--2-1_1">1:1</option>
                    <option value="aR--2-4_3">4:3</option>
                    <option value="aR--2-3_2">3:2</option>
                    <option value="aR--2-2_1">2:1</option>
                    <option value="aR--2-16_9">16:9</option>
                    <option value="aR--2-21_9">21:9</option>
                    <option value="aR--2-21_5">21:5</option>
                    <option value="aR--2-2_3">2:3</option>
                    <option value="aR--2-3_4">3:4</option>
                </select>

                <div class="edit ratio_2">
                    <div class="display_none">
                        <?= $this->input("ratio_2"); ?>
                    </div>
                </div>

                <div class="edit nogo">
                    <div class="pimcore_tag_checkbox">
                        <input name="app__checkbox_nogo"
                               @change="checkbox($event)"
                               data-target="nogo"
                               value="1"
                               data-value="nogo"
                               type="checkbox"
                               v-model="nogo" />
                    </div>
                    <div class="display_none">
                        <?= $this->input("nogo"); ?>
                    </div>
                </div>

            </div>

        </div>

        <div class="tx-kitt3n-image imgWrapper edit" id="edit--<?= $sUid; ?>">

            <div id="aR--<?= $sUid; ?>"
                 class="aR <?= $this->input('ratio_0')->getData(); ?> <?= $this->input('ratio_1')->getData(); ?> <?= $this->input('ratio_2')->getData(); ?>">

                <div class="focusPoint"
                     style="top: <?= $sTop; ?>; left: <?= $sLeft; ?>;">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                        <path fill="#ffffff" fill-rule="evenodd"
                              d="M152,214 L156.508995,214 C156.780169,214 157,214.231934 157,214.5 C157,214.776142 156.77212,215 156.508995,215 L151.491005,215 C151.356934,215 151.235414,214.943306 151.146806,214.853582 C151.054763,214.760658 151,214.639137 151,214.508995 L151,209.491005 C151,209.219831 151.231934,209 151.5,209 C151.776142,209 152,209.22788 152,209.491005 L152,214 Z M168,214 L163.491005,214 C163.22788,214 163,214.223858 163,214.5 C163,214.768066 163.219831,215 163.491005,215 L168.508995,215 C168.639129,215 168.760642,214.945244 168.850231,214.856488 C168.9433,214.7646 169,214.643073 169,214.508995 L169,209.491005 C169,209.22788 168.776142,209 168.5,209 C168.231934,209 168,209.219831 168,209.491005 L168,214 Z M168.853194,197.146418 C168.764586,197.056694 168.643066,197 168.508995,197 L163.491005,197 C163.22788,197 163,197.223858 163,197.5 C163,197.768066 163.219831,198 163.491005,198 L168,198 L168,202.508995 C168,202.77212 168.223858,203 168.5,203 C168.768066,203 169,202.780169 169,202.508995 L169,197.491005 C169,197.360863 168.945237,197.239342 168.856471,197.149752 Z M151.149769,197.143512 C151.239358,197.054756 151.360871,197 151.491005,197 L156.508995,197 C156.780169,197 157,197.231934 157,197.5 C157,197.776142 156.77212,198 156.508995,198 L152,198 L152,202.508995 C152,202.780169 151.768066,203 151.5,203 C151.223858,203 151,202.77212 151,202.508995 L151,197.491005 C151,197.356927 151.0567,197.2354 151.146433,197.146791 Z M160,208 C161.104569,208 162,207.104569 162,206 C162,204.895431 161.104569,204 160,204 C158.895431,204 158,204.895431 158,206 C158,207.104569 158.895431,208 160,208 Z"
                              transform="translate(-151 -197)"/>
                    </svg>
                </div>

                <style>
                    #edit--<?= $sUid; ?> img {
                        object-position: <?= $sPosition ?>;
                    }
                </style>

                <?= $this->image(
                    "image",
                    [
                        "thumbnail" => [
                            "width" => 1920,
                            "interlace" => true,
                            "quality" => 90,
                        ],
                        "disableInlineUpload" => true,
                        "cacheBuster" => false,
                        "reload" => true,
                    ]
                ); ?>
            </div>

        </div>


    <?php else: ?>

        <?php if (!$this->image("image")->isEmpty()): ?>

            <?php

            $aProcessedImageCsss = [];

            /*
             * Fallback for
             * - IEs 9 and lower
             * - Edge 12-15
             */
            $aProcessedImageFallbackCsss = [];

            /*
             * Fallback for
             * - IEs 10 and 11
             */
            $aProcessedImageIE10PlusFallbackCsss = [];

            $aProcessedImageFallbackCsss[] =
                '#garfield--' . $sUid . '.tx-kitt3n-image > .aR > img { display: none;}';

            $aProcessedImageIE10PlusFallbackCsss[] =
                '@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {' .
                '#garfield--' . $sUid . '.tx-kitt3n-image > .aR > img { display: none; }}';

            $aProcessedImageCsss[] =
                '#garfield--' . $sUid . '.tx-kitt3n-image > .aR > img { object-position:' . $sPosition . '; }';

            $aProcessedImageFallbackCsss[] =
                '#garfield--' . $sUid . '.tx-kitt3n-image > .aR { background: url(' .
                $this->image("image")->getThumbnail([
                    "width" => 992,
                    "interlace" => true,
                    "quality" => 90,
                ]) .
                ') no-repeat; background-size: cover; background-position: ' . $sPosition . '; }';

            $aProcessedImageIE10PlusFallbackCsss[] =
                '@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {' .
                '#garfield--' . $sUid . '.tx-kitt3n-image > .aR { background: url(' .
                $this->image("image")->getThumbnail([
                    "width" => 992,
                    "interlace" => true,
                    "quality" => 90,
                ]) .
                ') no-repeat; background-size: cover; background-position: ' . $sPosition . '; }}';

            $aProcessedImageFallbackCsss[] =
                '@media (min-width: 992px) and (max-width: 1199px) {' .
                '#garfield--' . $sUid . '.tx-kitt3n-image > .aR { background: url(' .
                $this->image("image")->getThumbnail([
                    "width" => 1200,
                    "interlace" => true,
                    "quality" => 90,
                ]) .
                ') no-repeat; background-size: cover; background-position: ' . $sPosition . '; }}';

            $aProcessedImageIE10PlusFallbackCsss[] =
                '@media (min-width: 992px) and (max-width: 1199px) and (-ms-high-contrast: none), (-ms-high-contrast: active) {' .
                '#garfield--' . $sUid . '.tx-kitt3n-image > .aR { background: url(' .
                $this->image("image")->getThumbnail([
                    "width" => 1200,
                    "interlace" => true,
                    "quality" => 90,
                ]) .
                ') no-repeat; background-size: cover; background-position: ' . $sPosition . '; }}';

            $aProcessedImageFallbackCsss[] =
                '@media (min-width: 1200px) {' .
                '#garfield--' . $sUid . '.tx-kitt3n-image > .aR { background: url(' .
                $this->image("image")->getThumbnail([
                    "width" => 1920,
                    "interlace" => true,
                    "quality" => 90,
                ]) .
                ') no-repeat; background-size: cover; background-position: ' . $sPosition . '; }}';

            $aProcessedImageIE10PlusFallbackCsss[] =
                '@media (min-width: 1200px) and (-ms-high-contrast: none), (-ms-high-contrast: active) {' .
                '#garfield--' . $sUid . '.tx-kitt3n-image > .aR { background: url(' .
                $this->image("image")->getThumbnail([
                    "width" => 1920,
                    "interlace" => true,
                    "quality" => 90,
                ]) .
                ') no-repeat; background-size: cover; background-position: ' . $sPosition . '; }}';
            ?>

            <style><?= implode(" ", $aProcessedImageCsss); ?></style>
            <!-- IE 9 and lower -->
            <!--[if lt IE 10]>
            <style type="text/css" media="screen"><?= implode(" ", $aProcessedImageFallbackCsss); ?></style><![endif]-->
            <!-- IE 10+ -->
            <style><?= implode(" ", $aProcessedImageIE10PlusFallbackCsss); ?></style>
            <!-- Edge 12-15 -->
            <style>@supports (-ms-ime-align:auto) and (not (display: grid)) {
                <?= implode(" ", $aProcessedImageFallbackCsss); ?>
                }</style>
            <figure class="tx-kitt3n-image" id="garfield--<?= $sUid ?>">
                <picture
                        class="aR <?= $this->input('ratio_0')->getData(); ?> <?= $this->input('ratio_1')->getData(); ?> <?= $this->input('ratio_2')->getData(); ?>">

                    <source media="(min-width: 1200px)"
                            data-srcset="<?= $this->image("image")->getThumbnail([
                                "width" => 1920,
                                "interlace" => true,
                                "quality" => 90,
                            ]); ?>">

                    <source media="(min-width: 992px)"
                            data-srcset="<?= $this->image("image")->getThumbnail([
                                "width" => 1200,
                                "interlace" => true,
                                "quality" => 90,
                            ]); ?>">

                    <img class="lazy"
                         alt="<?= $this->image("image")->getAlt(); ?>"
                         title="<?= $this->image("image")->getAlt(); ?>"
                         data-src="<?= $this->image("image")->getThumbnail([
                             "width" => 992,
                             "interlace" => true,
                             "quality" => 90,
                         ]); ?>">

                    <noscript>
                        <img alt="<?= $this->image("image")->getAlt(); ?>"
                             title="<?= $this->image("image")->getAlt(); ?>"
                             src="<?= $this->image("image")->getThumbnail([
                                 "width" => 992,
                                 "interlace" => true,
                                 "quality" => 90,
                             ]); ?>">
                    </noscript>

                </picture>
            </figure>

        <?php endif; ?>

    <?php endif; ?>

</div>

<?php if ($this->editmode): ?>

    <script>

        var vueapp_<?= $sUid; ?> = new Vue({
            el: "#app--<?= $sUid; ?>",
            data: {
                ratio0: "<?= $this->input('ratio_0')->getData(); ?>",
                ratio1: "<?= $this->input('ratio_1')->getData(); ?>",
                ratio2: "<?= $this->input('ratio_2')->getData(); ?>",
                nogo: "<?= $this->input('nogo')->getData(); ?>"
            },
            methods: {
                onchange: function (event) {

                    var oEventSource = event.target;
                    var sDataEventTarget = oEventSource.getAttribute('data-target');

                    var oApp = this.$el;

                    // Get pimcore input element inside app div by data-real-name attribute
                    // and set new value
                    //
                    var oEventTarget = oApp.querySelector('[data-real-name="' + sDataEventTarget + '"]');

                    oEventTarget.innerHTML = event.target.value;

                    // Change class on aR container for preview
                    //
                    var oEdit = oApp.nextElementSibling;
                    var oWrapper = oEdit.querySelector('[id="aR--<?= $sUid; ?>"]');

                    var sCurrentClasses = oWrapper.getAttribute('class');
                    var aDataOptions = oEventSource.getAttribute('data-options').split(',');
                    for (var i = 0; i < aDataOptions.length; i++) {
                        sCurrentClasses = sCurrentClasses.replace(' ' + aDataOptions[i], '');
                        sCurrentClasses = sCurrentClasses.replace(aDataOptions[i], '');
                    }
                    oWrapper.setAttribute('class', sCurrentClasses + ' ' + event.target.value);

                },
                checkbox: function(event) {

                    var oEventSource = event.target;
                    var sDataEventTarget = oEventSource.getAttribute('data-target');

                    var oApp = this.$el;

                    //
                    // Get pimcore input element inside app div by data-real-name attribute
                    // and set new value
                    //
                    var oEventTarget = oApp.querySelector('[data-real-name="' + sDataEventTarget + '"]');

                    var sValue = '';
                    switch (true) {
                        case event.target.checked === true:
                            sValue = 1;
                            break;
                    }

                    oEventTarget.innerHTML = sValue;

                    //
                    // Add/Remove nogo class to #edit... div when clicking the checkbox
                    //
                    var oElement = document.getElementById("element--<?= $sUid ?>");
                    var sCurrentClasses = oElement.getAttribute('class');
                    var sDataValue = oEventSource.getAttribute('data-value');
                    sCurrentClasses = sCurrentClasses.replace(' ' + sDataValue, '');
                    sCurrentClasses = sCurrentClasses.replace(sDataValue, '');
                    if (sValue !== '') {
                        sCurrentClasses += ' ' + sDataValue;
                    }
                    oElement.setAttribute('class' ,sCurrentClasses);

                }

            }
        })

    </script>

<?php endif; ?>
