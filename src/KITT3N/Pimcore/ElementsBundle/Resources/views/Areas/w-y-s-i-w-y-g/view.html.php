<?php

use Pimcore\Model\Document\Tag\Area\Info;

/* @var Pimcore\Model\Document\Tag\Area\Info $info */
$aGlobalParams = $info->getParams();

/* @var Pimcore\Model\Document\Tag\Area\Info $brick */
$cTag = $brick->getTag();

/* Unique identifier */
$iCurrentIndex = $cTag->currentIndex['key'];

/*
 * $cTag->currentIndex['key'] is available if element is an "areablock"
 * If element is an "area" use $this->suffix cause $cTag->currentIndex['key'] is null here
 */
$sSuffix = ($iCurrentIndex === null ? '': '_' . $iCurrentIndex) . (isset($this->suffix) ? '_' . $this->suffix : '');

$sUid = $aGlobalParams["uid"] . '_' . $aGlobalParams["col"] . $sSuffix;

?>

<div id="element--<?= $sUid ?>"
     class="element element--wysiwyg<?php if ($this->editmode): ?> edit<?php endif; ?>">

<?= $this->wysiwyg("wysiwyg", [
    "customConfig" => "/bundles/kitt3npimcoreelements/js/pimcore/ckeditor_wysiwyg.js",
    "height" => "auto",
//    "toolbarGroups" => [
//        [
//            "name" => 'basicstyles',
//            "groups" => [ 'basicstyles', 'list', "links", "styles"]
//        ]
//    ],
    "placeholder" =>
        "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum."
]); ?>

</div>
