<?php use Pimcore\Model\Asset;

if ($this->getParam("editmode")): ?>

    <?php if ("" === $this->sEditmode): ?>
        <?php
        throw new Exception("Please drop a 'folder'. Otherwise clowns will eat you!");
        ?>
    <?php else: ?>
        <p><?= $this->sEditmode; ?></p>
    <?php endif; ?>

<?php else: ?>

    <!-- Render assets -->

    <?php $aRestrictions = [];
        if ($this->oUser instanceof \AppBundle\Model\DataObject\User): ?>
        <?php
        /* @var array|null $xRestrictions */
        $xRestrictions = $oUser->getGroup()->getAssetRestrictions();
        $aRestrictions = $xRestrictions === null ? [] : $xRestrictions;
        ?>
    <?php endif; ?>

    <?php if ( ! empty($this->aMessages)): ?>
        <?php foreach($this->aMessages as $aMessage): ?>
            <div class="message message--<?= $aMessage["type"]; ?>">
                <?= $aMessage["message"]; ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if ( ! empty($this->aCrumbs)): ?>
        <div class="breadcrumb">
            <?php foreach($this->aCrumbs as $aCrumb): ?>
                <?php if($aCrumb === $this->aCrumbs[count($this->aCrumbs)-1]): ?>
                    <span>
            <?= $aCrumb["label"]; ?>
        </span>
                <?php else: ?>
                    <a href="?id=<?= $aCrumb["id"]; ?>">
                        <?= $aCrumb["label"]; ?>
                    </a>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <?php if ( ! empty($this->aListOfAssets)): ?>
        <?php /* @var Pimcore\Model\Asset $oAsset */ ?>
        <div class="wrapper margin_2 margin--m">

            <div class="grid-container c3-c3-c3-c3">
                <?php foreach ($this->aListOfAssets as $iKey => $oAsset): ?>

                    <?php if ($oAsset->getType() === 'folder'): ?>
                        <div class="column">
                            <a class="asset"
                               href="?id=<?= $oAsset->getId(); ?>"
                               alt="<?= $oAsset->getFilename(); ?>">
                                <figure class="type--<?= $oAsset->getType(); ?>">
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=">
                                    <figcaption>
                                        <?= $oAsset->getFilename(); ?>
                                        <span>
                                    <?php $iCountChildren = count($oAsset->getChildren()) ?>
                                    <?= ($iCountChildren === 0 ? '-' : ($iCountChildren === 1 ?
                                        $iCountChildren . ' ' . $this->translate("ElementsBundle__asset_simpleFileBrowser_element") :
                                        $iCountChildren . ' ' . $this->translate("ElementsBundle__asset_simpleFileBrowser_elements"))); ?>
                                </span>
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    <?php elseif ($oAsset->getType() === 'image'): ?>
                        <div class="column">
                            <label for="activate-overlay"
                                   class="asset activate-overlay--<?= $this->getParam("sUid"); ?>"
                                   href="/kitt3npimcoreelements/simple-file-viewer/asset/<?= $oAsset->getId(); ?>"
                                   target="_blank"
                                   alt="<?= $oAsset->getFilename(); ?>"
                                   title="<?= $oAsset->getFilename(); ?>">
                                <figure class="file type--<?= $oAsset->getType(); ?>">
                                    <?php if ($oAsset->getWidth() >= $oAsset->getHeight()): ?>
                                        <img class="lazy landscape"
                                             data-srcset="<?= $oAsset->getThumbnail('landscape-256'); ?>" />
                                    <?php else: ?>
                                        <img class="lazy portrait"
                                             data-srcset="<?= $oAsset->getThumbnail('portrait-256'); ?>" />
                                    <?php endif; ?>
                                    <figcaption>
                                        <?= $oAsset->getFilename(); ?>
                                        <span>
                                    <?= $oAsset->getFilesize(true, 2) ; ?>
                                </span>
                                    </figcaption>
                                </figure>
                            </label>

                            <?php if ( ! in_array("download", $aRestrictions)): ?>
                                <a class="download"
                                   href="/kitt3npimcoreelements/simple-file-downloader/asset/<?= $oAsset->getId(); ?>">dl</a>
                            <?php endif; ?>

                        </div>
                    <?php elseif ($oAsset->getType() === 'document'): ?>
                        <div class="column">
                            <span class="asset">
                                <figure class="type--<?= $oAsset->getType(); ?>">
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=">
                                    <figcaption>
                                        <?= $oAsset->getFilename(); ?>
                                        <span>
                                            <?= $oAsset->getFilesize(true, 2) ; ?>
                                        </span>
                                    </figcaption>
                                </figure>
                            </span>

                            <?php if ( ! in_array("download", $aRestrictions)): ?>
                                <a class="download"
                                   href="/kitt3npimcoreelements/simple-file-downloader/asset/<?= $oAsset->getId(); ?>">dl</a>
                            <?php endif; ?>

                        </div>
                    <?php elseif ($oAsset->getType() === 'text'): ?>
                        <div class="column">
                            <span class="asset">
                                <figure class="type--<?= $oAsset->getType(); ?>">
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=">
                                    <figcaption>
                                        <?= $oAsset->getFilename(); ?>
                                        <span>
                                            <?= $oAsset->getFilesize(true, 2) ; ?>
                                        </span>
                                    </figcaption>
                                </figure>
                            </span>

                            <<?php if ( ! in_array("download", $aRestrictions)): ?>
                                <a class="download"
                                   href="/kitt3npimcoreelements/simple-file-downloader/asset/<?= $oAsset->getId(); ?>">dl</a>
                            <?php endif; ?>

                        </div>
                    <?php else: ?>
                        <div class="column">
                            <span class="asset">
                                <figure class="type--<?= $oAsset->getType(); ?>">
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=">
                                    <figcaption>
                                        <?= $oAsset->getFilename(); ?>
                                        <span>
                                            <?= $oAsset->getFilesize(true, 2) ; ?>
                                        </span>
                                    </figcaption>
                                </figure>
                            </span>

                            <?php if ( ! in_array("download", $aRestrictions)): ?>
                                <a class="download"
                                   href="/kitt3npimcoreelements/simple-file-downloader/asset/<?= $oAsset->getId(); ?>">dl</a>
                            <?php endif; ?>

                        </div>
                    <?php endif; ?>

                    <?php if (($iKey+1)%4 === 0): ?>
                </div>
                <div class="grid-container c3-c3-c3-c3">
                    <?php endif; ?>

                <?php endforeach; ?>
            </div>

        </div>
        <script>
            var aXHR_<?= $this->getParam("sUid"); ?> = [];
            var aActivateOverlayLabels_<?= $this->getParam("sUid"); ?> = document.getElementsByClassName('activate-overlay--<?= $this->getParam("sUid"); ?>');
            if (aActivateOverlayLabels_<?= $this->getParam("sUid"); ?>.length > 0) {
                for (var i = 0; i < aActivateOverlayLabels_<?= $this->getParam("sUid"); ?>.length; i++) {
                    (function(index) {
                        aActivateOverlayLabels_<?= $this->getParam("sUid"); ?>[index].addEventListener('click', function(e) {
                            var oOverlayContent = document.getElementById('overlay-content');
                            oOverlayContent.innerHTML = '';
                            aXHR_<?= $this->getParam("sUid"); ?>[index] = new XMLHttpRequest();
                            aXHR_<?= $this->getParam("sUid"); ?>[index].addEventListener("load", function (event) {
                                oOverlayContent.innerHTML = event.target.responseText;
                            });
                            aXHR_<?= $this->getParam("sUid"); ?>[index].addEventListener("error", function (event) {
                                oOverlayContent.innerHTML = event.target.responseText;
                            });
                            aXHR_<?= $this->getParam("sUid"); ?>[index].open("GET", this.getAttribute('href'), true);
                            aXHR_<?= $this->getParam("sUid"); ?>[index].send();
                        });
                    })(i);
                }
            }
        </script>
    <?php endif; ?>

    <!-- / Render assets -->

<?php endif; ?>
