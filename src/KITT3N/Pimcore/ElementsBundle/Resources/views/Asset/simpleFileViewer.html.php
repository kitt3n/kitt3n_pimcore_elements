<?php /* @var \Pimcore\Model\Asset $oAsset */ ?>
<div id="element--<?= $oAsset->getId() ?>"
     class="element element--assets element--simple-file-viewer">

<?php if ($oAsset->getType() === 'image'): ?>
    <div class="grid-container c8-c4">
        <div class="column">
            <figure class="file type--<?= $oAsset->getType(); ?>">
                <?php if ($oAsset->getWidth() >= $oAsset->getHeight()): ?>
                    <img class="lazy landscape"
                         src="<?= $oAsset->getThumbnail('landscape-800'); ?>" />
                <?php else: ?>
                    <img class="lazy portrait"
                         src="<?= $oAsset->getThumbnail('portrait-800'); ?>" />
                <?php endif; ?>
                <figcaption>
                    <?= $oAsset->getFilename(); ?>
                    <span>
                                    <?= $oAsset->getFilesize(true, 2) ; ?>
                                </span>
                </figcaption>
            </figure>
        </div>
        <div class="column">
            <a class="download"
               href="/kitt3npimcoreelements/simple-file-downloader/asset/<?= $oAsset->getId(); ?>">dl</a>

            <?php if( ! empty($aTagsForCurrentAsset)): ?>

                <?php foreach ($aTagsForCurrentAsset as $oTag): ?>
                    <?= $oTag->getName(); ?><br>
                <?php endforeach; ?>

            <?php endif; ?>
        </div>
    </div>

<?php endif; ?>

</div>
