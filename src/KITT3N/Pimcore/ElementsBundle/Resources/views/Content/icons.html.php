<?php
/** @var \Pimcore\Templating\PhpEngine $this */

use Pimcore\Model\Asset\Image;

?>
<?php if ($this->assets): ?>
    <div class="grid-container c3-c3-c3-c3">

        <?php

        switch (true):
            case count($this->svg):

                $i = 1;
                foreach ($this->svg as $asset):

                    if ($asset instanceof Pimcore\Model\Asset\Image):
                        /** @var Pimcore\Model\Asset\Image $asset */

                        ?>

                        <div class="column">
                            <figure>
                           <?= $asset->getThumbnail()->getHtml(); ?>
                                <figcaption>
                                    <?= ucfirst(
                                            str_replace(
                                                    ['-', '.svg'],
                                                    [' ', ''],
                                                    $asset->getFilename()
                                            )
                                    ); ?>
                                </figcaption>
                            </figure>
                        </div>


                        <?php

                        if ($i%4 === 0):

                            ?>

                            </div><div class="grid-container c3-c3-c3-c3">

                            <?php


                        endif;

                        $i++;

                    endif;

                endforeach;

                break;

        endswitch;

        ?>

    </div>
<?php endif; ?>