<div class="edit column_sizes">
    <div class="display_none">
        <?= $this->input("column_sizes"); ?>
    </div>
</div>

<select name="app__select_padding_size"
        @change="onchange($event)"
        data-target="padding_size"
        data-target-element="wrapper"
        data-options="padding--s,padding--m,padding--l"
        v-model="paddingSize"
        title="Innen-Abstand">
    <option value="padding--s">S</option>
    <option value="padding--m">M</option>
    <option value="padding--l">L</option>
</select>

<select name="app__select_margin_size"
        @change="onchange($event)"
        data-target="margin_size"
        data-target-element="wrapper"
        data-options="margin--s,margin--m,margin--l"
        v-model="marginSize"
        title="Außen-Abstand">
    <option value="margin--s">S</option>
    <option value="margin--m">M</option>
    <option value="margin--l">L</option>
</select>

<div class="edit padding_size">
    <div class="display_none">
        <?= $this->input("padding_size"); ?>
    </div>
</div>

<div class="edit margin_size">
    <div class="display_none">
        <?= $this->input("margin_size"); ?>
    </div>
</div>