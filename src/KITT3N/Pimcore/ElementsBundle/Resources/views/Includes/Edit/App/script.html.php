<?php if($this->editmode): ?>

    <script>

        var vueapp_<?php echo $cIndex; ?> = new Vue({
            el: "#app--<?php echo$cIndex; ?>",
            data: {
                columnSizes: "<?php echo $this->input('column_sizes')->getData(); ?>",
                paddingSize: "<?php echo $this->input('padding_size')->getData(); ?>",
                marginSize: "<?php echo $this->input('margin_size')->getData(); ?>",
                padding0: "<?php echo $this->input('padding_0')->getData(); ?>",
                padding1: "<?php echo $this->input('padding_1')->getData(); ?>",
                padding2: "<?php echo $this->input('padding_2')->getData(); ?>",
                padding3: "<?php echo $this->input('padding_3')->getData(); ?>",
                margin2: "<?php echo $this->input('margin_2')->getData(); ?>"
            },
            methods: {
                onchange: function(event) {

                    var oEventSource = event.target;
                    var sDataEventTarget = oEventSource.getAttribute('data-target');
                    var sDataEventTargetElement = oEventSource.getAttribute('data-target-element');

                    var oApp = this.$el;
                    var oWrapper = document.getElementById("wrapper--<?php echo $cIndex; ?>");
                    var oGridContainer = oWrapper.querySelector('[id="grid-container--<?php echo $cIndex; ?>"]');

                    //
                    // Get pimcore input element inside app div by data-real-name attribute
                    // and set new value
                    //
                    var oEventTarget = oApp.querySelector('[data-real-name="' + sDataEventTarget + '"]');

                    oEventTarget.innerHTML = event.target.value;

                    switch (true) {
                        case sDataEventTargetElement === 'wrapper':

                            var sCurrentClasses = oWrapper.getAttribute('class');
                            var aDataOptions = oEventSource.getAttribute('data-options').split(',');
                            for (var i = 0; i < aDataOptions.length; i++) {
                                sCurrentClasses = sCurrentClasses.replace(' ' + aDataOptions[i], '');
                                sCurrentClasses = sCurrentClasses.replace(aDataOptions[i], '');
                            }
                            oWrapper.setAttribute('class' ,sCurrentClasses + ' ' + event.target.value);

                            break;
                        case sDataEventTargetElement === 'grid-container':

                            var sCurrentClasses = oGridContainer.getAttribute('class');
                            var aDataOptions = oEventSource.getAttribute('data-options').split(',');
                            for (var i = 0; i < aDataOptions.length; i++) {
                                sCurrentClasses = sCurrentClasses.replace(' ' + aDataOptions[i], '');
                                sCurrentClasses = sCurrentClasses.replace(aDataOptions[i], '');
                            }
                            oGridContainer.setAttribute('class' ,sCurrentClasses + ' ' + event.target.value);

                            break;
                    }

                },
                checkbox: function(event) {

                    var oEventSource = event.target;
                    var sDataEventTarget = oEventSource.getAttribute('data-target');
                    var sDataEventTargetElement = oEventSource.getAttribute('data-target-element');

                    var oApp = this.$el; console.log(oApp);
                    var oWrapper = document.getElementById("wrapper--<?php echo $cIndex; ?>");
                    var oGridContainer = oWrapper.querySelector('[id="grid-container--<?php echo $cIndex; ?>"]');

                    //
                    // Get pimcore input element inside app div by data-real-name attribute
                    // and set new value
                    //
                    var oEventTarget = oApp.querySelector('[data-real-name="' + sDataEventTarget + '"]');

                    var sValue = '';
                    switch (true) {
                        case event.target.checked === true:
                            sValue = 1;
                            break;
                    }

                    oEventTarget.innerHTML = sValue;

                    switch (true) {
                        case sDataEventTargetElement === 'wrapper':

                            var sCurrentClasses = oWrapper.getAttribute('class');
                            var sDataValue = oEventSource.getAttribute('data-value');
                            sCurrentClasses = sCurrentClasses.replace(' ' + sDataValue, '');
                            sCurrentClasses = sCurrentClasses.replace(sDataValue, '');
                            if (sValue !== '') {
                                sCurrentClasses += ' ' + sDataValue;
                            }
                            oWrapper.setAttribute('class' ,sCurrentClasses);

                            break;
                        case sDataEventTargetElement === 'grid-container':

                            var sCurrentClasses = oGridContainer.getAttribute('class');
                            var sDataValue = oEventSource.getAttribute('data-value');
                            sCurrentClasses = sCurrentClasses.replace(' ' + sDataValue, '');
                            sCurrentClasses = sCurrentClasses.replace(sDataValue, '');
                            if (sValue !== '') {
                                sCurrentClasses += ' ' + sDataValue;
                            }
                            oGridContainer.setAttribute('class' ,sCurrentClasses);

                            break;
                    }


                }
            }
        })

    </script>

<?php endif; ?>