<div class="edit margin margin_2">
    <div class="pimcore_tag_checkbox">
        <input name="app__checkbox_margin_2"
               @change="checkbox($event)"
               data-target="margin_2"
               data-target-element="wrapper"
               value="1"
               data-value="margin_2"
               type="checkbox"
               v-model="margin2" />
    </div>
    <div class="display_none">
        <?= $this->input("margin_2"); ?>
    </div>
</div>

<div class="edit padding padding_0">
    <div class="pimcore_tag_checkbox">
        <input name="app__checkbox_padding_0"
               @change="checkbox($event)"
               data-target="padding_0"
               data-target-element="wrapper"
               value="1"
               data-value="padding_0"
               type="checkbox"
               v-model="padding0" />
    </div>
    <div class="display_none">
        <?= $this->input("padding_0"); ?>
    </div>
</div>
<div class="edit padding padding_1">
    <div class="pimcore_tag_checkbox">
        <input name="app__checkbox_padding_1"
               @change="checkbox($event)"
               data-target="padding_1"
               data-target-element="wrapper"
               value="1"
               data-value="padding_1"
               type="checkbox"
               v-model="padding1" />
    </div>
    <div class="display_none">
        <?= $this->input("padding_1"); ?>
    </div>
</div>
<div class="edit padding padding_2">
    <div class="pimcore_tag_checkbox">
        <input name="app__checkbox_padding_2"
               @change="checkbox($event)"
               data-target="padding_2"
               data-target-element="wrapper"
               value="1"
               data-value="padding_2"
               type="checkbox"
               v-model="padding2" />
    </div>
    <div class="display_none">
        <?= $this->input("padding_2"); ?>
    </div>
</div>
<div class="edit padding padding_3">
    <div class="pimcore_tag_checkbox">
        <input name="app__checkbox_padding_3"
               @change="checkbox($event)"
               data-target="padding_3"
               data-target-element="wrapper"
               value="1"
               data-value="padding_3"
               type="checkbox"
               v-model="padding3" />
    </div>
    <div class="display_none">
        <?= $this->input("padding_3"); ?>
    </div>
</div>