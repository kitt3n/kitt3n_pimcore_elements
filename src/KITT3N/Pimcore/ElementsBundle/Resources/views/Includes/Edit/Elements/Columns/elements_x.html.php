<?= $this->areablock('elements_' . (isset($col) ? $col : 0), [
    'allowed' => [
        'w-y-s-i-w-y-g',
        'picture',
        'picture-and-text',
        'picture-beside-text',
        'image',
        'color',
        'icons',
        'assets',
        'asset-download',
    ],
    'sorting' => [
        'w-y-s-i-w-y-g',
        'picture',
        'picture-and-text',
        'picture-beside-text',
        'image',
        'color',
        'icons',
        'assets',
        'asset-download',
    ],
    "globalParams" => [
        "uid" => $this->uid,
        "col" => $this->col,
        "aBamParams" => $this->aBamParams
    ],
]); ?>