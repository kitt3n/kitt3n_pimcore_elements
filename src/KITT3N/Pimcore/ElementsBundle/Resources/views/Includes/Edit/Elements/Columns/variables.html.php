<?php

/* @var string $sClasses */

use Pimcore\Model\Document\Tag\Area\Info;

$sClasses = '';

/* @var Pimcore\Model\Document\Tag\Area\Info $brick */
$cTag = $brick->getTag();

/* Unique identifier */
$iCurrentIndex = $cTag->currentIndex['key'];

/*
* Padding
* - 0 / top
* - 1 / right
* - 2 / bottom
* - 3 / left
*/
if ( ! $this->input("padding_0")->isEmpty()) {
    $sClasses .= ' padding_0';
}
if ( ! $this->input("padding_1")->isEmpty()) {
    $sClasses .= ' padding_1';
}
if ( ! $this->input("padding_2")->isEmpty()) {
    $sClasses .= ' padding_2';
}
if ( ! $this->input("padding_3")->isEmpty()) {
    $sClasses .= ' padding_3';
}

/*
* Padding size
* - S
* - M
* - L
*/
if($this->input("padding_size")->isEmpty()) {
    $this->input("padding_size")->setDataFromResource("padding--m");
}
$sClasses .= ' ' . $this->input("padding_size")->getData();

/*
* Margin
* - 2 / bottom
*/
if ( ! $this->input("margin_2")->isEmpty()) {
    $sClasses .= ' margin_2';
}

/*
* Margin size
* - S
* - M
* - L
*/
if($this->input("margin_size")->isEmpty()) {
    $this->input("margin_size")->setDataFromResource("margin--m");
}
$sClasses .= ' ' . $this->input("margin_size")->getData();

$this->input("wrapper_classes");
$this->input("wrapper_classes")->setDataFromResource($sClasses);

$this->input("unique_identifier");
$this->input("unique_identifier")->setDataFromResource($iCurrentIndex);